# base/build image

### arch

multi

* arm64/v8
* amd64

### included components

* alpine-sdk
* bunny-upload
* curl
* docker
* emm
* go
* golangci-lint
* govulncheck
* hugo
* just
* olm-dev
* skopeo
* swag

### updates

updated weekly

# base/app image

### arch

multi

* arm64/v8
* amd64

### included components

* ca-certificates
* tzdata
* olm

### updates

updated weekly
